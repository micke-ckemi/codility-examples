package io.mickeckemi21.lesson2_arrays;

import java.util.Arrays;

public class CyclicRotation {

	public static void main(String[] args) {
		
		// testing the CyclicRotation solution v1
		System.out.println("CyclicRotation the array {3, 8, 9, 7, 6} in 3 positions: " + Arrays.toString(solution(new int[] {3, 8, 9, 7, 6}, 3)));
		System.out.println("CyclicRotation the array {0, 0, 0} in 1 positions: " + Arrays.toString(solution(new int[] {0, 0, 0}, 1)));
		System.out.println("CyclicRotation the array {1, 2, 3, 4} in 4 positions: " + Arrays.toString(solution(new int[] {1, 2, 3, 4}, 4)));
		
		// testing the CyclicRotation solution v2
		System.out.println("CyclicRotation the array {3, 8, 9, 7, 6} in 3 positions: " + Arrays.toString(solution1(new int[] {3, 8, 9, 7, 6}, 3)));
		System.out.println("CyclicRotation the array {0, 0, 0} in 1 positions: " + Arrays.toString(solution1(new int[] {0, 0, 0}, 1)));
		System.out.println("CyclicRotation the array {1, 2, 3, 4} in 4 positions: " + Arrays.toString(solution1(new int[] {1, 2, 3, 4}, 4)));

	}
	
	public static int[] solution(int[] A, int K) {
		
		int size = A.length;
		int[] rotatedArray = new int[size];
		
	    if (K < 0 || K > 100 || size == 0) {
	        return rotatedArray;
	    }

	    if (size == 1) {
	        return A;
	    }
		
		for (int i = 0; i < size; i++) {
		
			rotatedArray[(i + K) % size] = A[i];
			
		}
		
		return rotatedArray;
	}
	
	public static int[] solution1(int[] A, int K) {
		
		int size = A.length;
		int[] rotatedArray = new int[size];
		
		if (size == 0 || K == 0) return A;
		
		if (K >= size) K %= size;

		for (int i = 0; i < size; i++) {
			rotatedArray[i] = (i < K) ? A[i + size - K] : A[i - K];
		}
		
		return rotatedArray;
	}

}
