package io.mickeckemi21.lesson2_arrays;

import java.util.HashSet;

public class OddOccurencesInArray {

	public static void main(String[] args) {
		
		// testing the OddOccurencesInArray solution v1
		System.out.println("OddOccurencesInArray in array {3, 8, 3, 8, 6}: " + solution(new int[] {3, 8, 3, 8, 6}));
		System.out.println("OddOccurencesInArray in array {0, 0, 0}: " + solution(new int[] {0, 0, 0}));
		System.out.println("OddOccurencesInArray in array {9, 3, 9, 3, 9, 7, 9}: " + solution(new int[] {9, 3, 9, 3, 9, 7, 9}));
		
		// testing the OddOccurencesInArray solution v1
		System.out.println("OddOccurencesInArray in array {3, 8, 3, 8, 6}: " + solution1(new int[] {3, 8, 3, 8, 6}));
		System.out.println("OddOccurencesInArray in array {0, 0, 0}: " + solution1(new int[] {0, 0, 0}));
		System.out.println("OddOccurencesInArray in array {9, 3, 9, 3, 9, 7, 9}: " + solution1(new int[] {9, 3, 9, 3, 9, 7, 9}));

	}
	
	public static int solution(int[] A) {
		
		int unpaired = 0;
		
		for (int number : A) {
			unpaired ^= number;
		}
		
		return unpaired;
		
	}
	
	public static int solution1(int[] A) {
		
		HashSet<Integer> set = new HashSet<>(A.length / 2);
		
		for (int i = 0; i < A.length; i++) {
			
			if (set.contains(A[i])) {
				
				set.remove(A[i]);
				
			} else {
				
				set.add(A[i]);
				
			}
			
		}
		
		return (int) set.toArray()[0];
		
	}

}
