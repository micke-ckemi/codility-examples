package io.mickeckemi21.lesson6_sorting;

import java.util.Arrays;

public class Triangle {

	public static void main(String[] args) {
		
		// testing the Triangle solution v1
        System.out.println("Triangle in array [10, 2, 5, 1, 8, 20] :  " + solution(new int[] {10, 2, 5, 1, 8, 20}));

	}
	
	public static int solution(int[] A) {
		
		Arrays.sort(A);
		
		for (int i = 0; i < A.length - 2; i++) {
			if (((long) A[i] + (long) A[i + 1] > A[i + 2]) &&
				((long) A[i + 1] + (long) A[i + 2] > A[i]) &&
				((long) A[i] + (long) A[i + 2] > A[i + 1])) return 1;

		}
		
		return 0;
		
	}

}
