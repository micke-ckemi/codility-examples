package io.mickeckemi21.lesson6_sorting;

public class NumberOfDiscIntersections {

	public static void main(String[] args) {
		
		// testing the NumberOfDiscIntersections solution v1
        System.out.println("NumberOfDiscIntersections in array [1, 5, 2, 1, 4, 0] :  " + solution(new int[] {1, 5, 2, 1, 4, 0}));
     
        // testing the NumberOfDiscIntersections solution v2
        System.out.println("NumberOfDiscIntersections in array [1, 5, 2, 1, 4, 0] :  " + solution1(new int[] {1, 5, 2, 1, 4, 0}));
        
        // testing the NumberOfDiscIntersections solution v3
        System.out.println("NumberOfDiscIntersections in array [1, 5, 2, 1, 4, 0] :  " + solution2(new int[] {1, 5, 2, 1, 4, 0}));

	}
	
	public static int solution(int[] A) {
		
		int n = A.length;
		int[] sum = new int[n];
		
		for (int i = 0; i < n; i++) {
			
			int right;
			//if i+A[i]<= n-1, that's it, extract this i+A[i], let sum[i+A[i]]++, means there is one disk that i+A[i]
			if (n - i - 1 >= A[i]){
                right = i + A[i];
            } else {
                right = n - 1;    
            }
            
            sum[right]++;
			
		}
		
		for (int i = 1; i < n; i++) {
            sum[i] += sum[i - 1];  //sum[i] means that there are sum[i] number of values that <= i;
        }
		
		long ans = (long) n * (n - 1) / 2;
		
		for (int i = 0; i < n; i++) {
            int left;
            
            if (A[i] > i) {
                left = 0;
            } else {
                left = i - A[i];// Find the positive i-A[i].     
            }
            
            if (left > 0){
                ans -= sum[left - 1];//Find the number that is smaller than 1-A[i], sum[n-1] will never be used as we only need sum[n-1-1] at most.  
            } 
        }
        
        if (ans > 10000000) {
            return -1;    
        }
        
        return (int) ans;
		
	} // O(N*log(N)) or O(N)
	
	public static int solution1(int[] A) {
        
        int[][] disc = new int[A.length][2];
        
        for(int i = 0; i < A.length; i++) {
            int downBound = i - A[i];
            int upBound = i + A[i];
            
            disc[i][0] = downBound;
            disc[i][1] = upBound;
 
        }
        
        int pairCounter = 0;
        for(int i = 0; i < A.length-1; i++) {
            
            for(int j = i + 1; j < A.length; j++) {
                if(disc[i][1] >= disc[j][0]) {
                    pairCounter++;
                }
                else {
                    //break;
                }
            }
        }
        
        return pairCounter;
    } // O(N * N)
	 
	public static int solution2(int[] A) {
		 
		 int result = 0;
		 int[] dps = new int[A.length];
		 int[] dpe = new int[A.length];
		 
		 for (int i = 0, t = A.length - 1; i < A.length; i++) {
			 
			 int s = i > A[i] ? i - A[i] : 0;
			 int e = t - i > A[i] ? i + A[i] : t;
			 dps[s]++;
			 dpe[e]++;
			 
		 }
		 
		 int t = 0;
		 for (int i = 0; i < A.length; i++) {
			 if (dps[i] > 0) {
				 result += t * dps[i];
				 result += dps[i] * (dps[i] - 1) / 2;
				 if (result > 10000000) return -1;
				 t += dps[i];
			 }
			 t -= dpe[i];
		 }
		 
		 return result;
		 
	 } // O(N)
	 
}
