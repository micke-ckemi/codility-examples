package io.mickeckemi21.lesson6_sorting;

import java.util.Arrays;

public class MaxProductOfThree {

	public static void main(String[] args) {
		
		// testing the MaxProductOfThree solution v1
        System.out.println("MaxProductOfThree in array [-3, 1, 2, -2, 5, 6] :  " + solution(new int[] {-3, 1, 2, -2, 5, 6}));
        
        // testing the MaxProductOfThree solution v1
        System.out.println("MaxProductOfThree in array [-5, 5, -5, 4] :  " + solution(new int[] {-5, 5, -5, 4}));

	}
	
	public static int solution(int[] A) {
		
		Arrays.sort(A);
		int F = 0, L = A.length - 1;
		int s1 = A[F] * A[F + 1] * A[F + 2];
		int s2 = A[F] * A[F + 1] * A[L];
		int s3 = A[F] * A[L - 1] * A[L];
		int s4 = A[L - 2] * A[L - 1] * A[L];
		
		return Math.max(Math.max(s1, s2), Math.max(s3, s4));
		
		
	}
}
