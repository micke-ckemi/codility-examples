package io.mickeckemi21.lesson6_sorting;

import java.util.Arrays;
import java.util.HashSet;

public class Distinct {

	public static void main(String[] args) {
		
		// testing the Distinct solution v1
        System.out.println("Distinct in array [2, 1, 1, 2, 3, 1] :  " + solution(new int[] {2, 1, 1, 2, 3, 1}));
        
        // testing the Distinct solution v2
        System.out.println("Distinct in array [2, 1, 1, 2, 3, 1] :  " + solution1(new int[] {2, 1, 1, 2, 3, 1}));

	}
	
	public static int solution(int[] A) {
		
		HashSet<Integer> set = new HashSet<>();
		
		for (int number : A) {
			set.add(number);
		}
		
		return set.size();
		
	}
	
	public static int solution1(int[] A) {
		
		int N = A.length;
		if (N == 0) {
			return 0;
		}
		
		// the built-in sorting function performs O(n*log(n)) complexity
		// even in the worst case
		Arrays.sort(A);
		int num = 1;
		int preDist = A[0];
		
		for (int i = 1; i < N; i++) {
			if (A[i] != preDist) {
				preDist = A[i];
				num++;
			}
		}
		
		return num;
		
	}

}
