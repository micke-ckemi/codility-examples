package io.mickeckemi21.lesson5_prefix_sums;

public class CountDiv {

	public static void main(String[] args) {
		
		// testing the CountDiv solution v1
        System.out.println("CountDiv with numbers A = 6; B = 11; K = 2 :  " + solution(6, 11, 2));
        // testing the CountDiv solution v1
        System.out.println("CountDiv with numbers A = 0; B = 1000; K = 1 :  " + solution(0, 1000, 1));

	}
	
	public static int solution(int A, int B, int K) {
		
		return B / K - A / K + (A % K == 0 ? 1 : 0);
		
	}

}
