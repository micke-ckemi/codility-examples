package io.mickeckemi21.lesson5_prefix_sums;

import java.util.Arrays;
import java.util.HashSet;

public class GenomicRangeQuery {

	public static void main(String[] args) {
		
		// testing the GenomicRangeQuery solution v1
		System.out.println("GenomicRangeQuery in String: CAGCCTA}: " + Arrays.toString(solution("CAGCCTA", new int[] {2, 5, 0}, new int[] {4, 5, 6})));
		// testing the GenomicRangeQuery solution v2
		System.out.println("GenomicRangeQuery in String: CAGCCTA}: " + Arrays.toString(solution1("CAGCCTA", new int[] {2, 5, 0}, new int[] {4, 5, 6})));

	}
	
	public static int[] solution(String S, int[] P, int[] Q) {
		
		int M = P.length;
		int[] result = new int[M];
		
		int i = 0;
		while (i < M) {
			
			HashSet<Character> charSet = new HashSet<>();
			
			char[] charArray = S.substring(P[i], Q[i] + 1).toCharArray();
			for (char ch : charArray) {
				charSet.add(ch);
			}
			
			if (charSet.contains('A')) {
				result[i] = 1;
			} else if (charSet.contains('C')) {
				result[i] = 2;
			} else if (charSet.contains('G')) {
				result[i] = 3;
			} else {
				result[i] = 4;
			}
			
			i++;
			
		}
		
		return result;
		
	} // O(N * M)

	public static int[] solution1(String S, int[] P, int[] Q) {

		// used jagged array to hold prefix sums of each A, C and G genoms
		// we don't need to get prefix sum of T
		int[][] genoms = new int[3][S.length() + 1];
		short a, c, g;
		for (int i = 0; i < S.length(); i++) {
			a = 0; c = 0; g = 0;
			if (S.charAt(i) == 'A') {
				a = 1;
			}
			if (S.charAt(i) == 'C') {
				c = 1;
			}
			if (S.charAt(i) == 'G') {
				g = 1;
			}

			// here we calculate prefix sums
			genoms[0][i + 1] = genoms[0][i] + a;
			genoms[1][i + 1] = genoms[1][i] + c;
			genoms[2][i + 1] = genoms[2][i] + g;
		}

		int[] result = new int[P.length];

		// here we go through provided P[] and Q[] arrays as intervals
		for (int i = 0; i < P.length; i++) {
			int fromIndex = P[i];
			int toIndex = Q[i] + 1;

			if (genoms[0][toIndex] - genoms[0][fromIndex] > 0) {
				result[i] = 1;
			} else if (genoms[1][toIndex] - genoms[1][fromIndex] > 0) {
				result[i] = 2;
			} else if (genoms[2][toIndex] - genoms[2][fromIndex] > 0) {
				result[i] = 3;
			} else {
				result[i] = 4;
			}
		}

		return result;

	} // O(N + M)


}
