package io.mickeckemi21.lesson5_prefix_sums;

public class PassingCars {

	public static void main(String[] args) {
		
		// testing the PassingCars solution v1
		System.out.println("PassingCars in array {0, 1, 0, 1, 1}: " + solution(new int[] {0, 1, 0, 1, 1}));
		// testing the PassingCars solution v1
		System.out.println("PassingCars in array {0, 0, 0, 0, 1, 1, 1, 1}: " + solution(new int[] {0, 0, 0, 0, 1, 1, 1, 1}));
		// testing the PassingCars solution v1
		System.out.println("PassingCars in array {0, 1, 0, 1, 0, 1, 0, 1}: " + solution(new int[] {0, 1, 0, 1, 0, 1, 0, 1}));
		// testing the PassingCars solution v1
		System.out.println("PassingCars in array {1, 1, 1, 1, 1, 1, 1, 1}: " + solution(new int[] {1, 1, 1, 1, 1, 1, 1, 1}));
		// testing the PassingCars solution v1
		System.out.println("PassingCars in array {0, 0, 0, 0, 0, 0, 0, 0}: " + solution(new int[] {0, 0, 0, 0, 0, 0, 0, 0}));

	}
	
	public static int solution(int[] A) {
		
		int increaseLevel = 0;
		int passingCars = 0;
		
		for (int number : A) {
			
			if (number == 0) {
				
				increaseLevel++;
				
			} else {
				
				passingCars += increaseLevel;
				
			}
			
			if (passingCars > 1000000000) {
				return -1;
			}
			
		}
		
		return passingCars;
		
	}

}
