package io.mickeckemi21.lesson7_stacks_and_queues;

import java.util.Stack;

public class Brackets {

	public static void main(String[] args) {
		
		// testing the Brackets solution v1
        System.out.println("Brackets in string '{[()()]}' :  " + solution("{[()()]}"));
        // testing the Brackets solution v1
        System.out.println("Brackets in string '([)()]' :  " + solution("([)()]"));

	}
	
	public static int solution(String S) {
		
		Stack<Character> leftBrackets = new Stack<Character>();
		for (int i = 0; i < S.length(); i++) {
			char ch = S.charAt(i);
			if (ch == ')' || ch == ']' || ch == '}') {
				if (leftBrackets.empty()) {
					return 0;
				}
				char top = leftBrackets.pop();
				if ((ch == ')' && top != '(') || (ch == ']' && top != '[') || (ch == '}' && top != '{')) {
					return 0;
				}
			} else {
				leftBrackets.push(ch);
			}

		}
		
		return leftBrackets.empty() ? 1 : 0;
		
	} // O(N)

}
