package io.mickeckemi21.lesson3_time_complexity;

public class FrogJmp {

	public static void main(String[] args) {
		
		// testing the FromJmp solution v1
		System.out.println("FromJmp for values X = 10, Y = 100, D = 30: " + solution(10, 100, 30));

	}
	
	public static int solution(int X, int Y, int D) {
		
		return (int) Math.ceil((double) (Y - X) / D);
		
	}

}
