package io.mickeckemi21.lesson3_time_complexity;

public class TapeEquilibrium {

	public static void main(String[] args) {
		
		// testing the TapeEquilibrium solution v1
		System.out.println("TapeEquilibrium for array [3, 1, 2, 4, 3]: " + solution(new int[] {3, 1, 2, 4, 3}));
		// testing the TapeEquilibrium solution v1
		System.out.println("TapeEquilibrium for array [3, 1, 6, 4, 3, 9, 13]: " + solution(new int[] {3, 1, 6, 4, 3, 9, 13}));
		// testing the TapeEquilibrium solution v1
		System.out.println("TapeEquilibrium for array [1, 1, 1, 1, 1, 1, 1]: " + solution(new int[] {1, 1, 1, 1, 1, 1, 1}));

	}
	
	public static int solution(int[] A) {
		
		int minDifference = Integer.MAX_VALUE;
		int arraySum = 0;
		int slideSum = 0;
		
		for (int i = 0; i < A.length; i++) {
			arraySum += A[i];
		}
		
		for (int i = 0; i < (A.length - 1); i++) {
			slideSum += A[i];
			minDifference = Math.min(minDifference, Math.abs((slideSum - (arraySum - slideSum))));
		}
		
		return minDifference;
		
	}

}
