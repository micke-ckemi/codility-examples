package io.mickeckemi21.lesson3_time_complexity;

import java.util.Arrays;
import java.util.stream.IntStream;

public class PermMissingElem {

	public static void main(String[] args) {
		
		// testing the PermMissingElem solution v1
		System.out.println("PermMissingElem for array [2, 3, 1, 5]: " + solution(new int[] {2, 3, 1, 5}));

		// testing the PermMissingElem solution v2
		System.out.println("PermMissingElem for array [2, 3, 1, 5]: " + solution1(new int[] {2, 3, 1, 5}));
		
		// testing the PermMissingElem solution v3
		System.out.println("PermMissingElem for array [2, 3, 1, 5]: " + solution2(new int[] {2, 3, 1, 5}));
		
		// testing the PermMissingElem solution v4
		System.out.println("PermMissingElem for array [2, 3, 1, 5]: " + solution3(new int[] {2, 3, 1, 5}));
		
	}
	
	public static int solution(int[] A) {
		
		int[] range = IntStream.iterate(1, n -> n + 1).limit(A.length + 1).toArray();
		return IntStream.of(range).sum() - IntStream.of(A).sum();
		
	}
	
	public static int solution1(int[] A) {
		
		long sum = 0;
		long arraySum = 0;
		long missing = 0;
		
		for (int i = 0; i < A.length; i++) {
			sum += (i + 1);
			arraySum += A[i];
		}
		
		sum += A.length + 1;
		missing = sum - arraySum;
		
		return (int) missing;
		
	}
	
	public static int solution2(int[] A) {
		
		long N = A.length + 1;
		long total = (N * (N + 1)) / 2;
		long sum = 0L;
		
		for (int i : A) {
			sum += i;
		}

		return (int) (total - sum);
		
	}
	
	public static int solution3(int[] A) {
		
		if (A == null) {
			return 0;
		}
		
		long arraySum = Arrays.stream(A).asLongStream().sum();
		long N = A.length + 1;
		long expectedSum = (N * (N + 1)) / 2;
		
		return (int) (expectedSum - arraySum);
		
	}

}
