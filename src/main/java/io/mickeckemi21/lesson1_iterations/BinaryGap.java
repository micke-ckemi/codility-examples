package io.mickeckemi21.lesson1_iterations;

public class BinaryGap {

	public static void main(String[] args) {
		
		// testing the BinaryGap solution v1
		System.out.println("BinaryGap for the number 1: " + solution(1));
		System.out.println("BinaryGap for the number 2: " + solution(2));
		System.out.println("BinaryGap for the number 57: " + solution(57));
		System.out.println("BinaryGap for the number 275: " + solution(275));
		System.out.println("BinaryGap for the number 533: " + solution(533));
		
		// testing the BinaryGap solution v2
		System.out.println("BinaryGap for the number 1: " + solution1(1));
		System.out.println("BinaryGap for the number 2: " + solution1(2));
		System.out.println("BinaryGap for the number 57: " + solution1(57));
		System.out.println("BinaryGap for the number 275: " + solution1(275));
		System.out.println("BinaryGap for the number 533: " + solution1(533));

	}
	
	// solution method v1
	public static int solution(int n) {
		
		// get rid of right-hand zeros
		while (n != 0 && (n & 1) == 0) {
			n >>>= 1;
		}
		
		int max = 0;
		int gap = 0;
		while (n != 0) {
			if ((n & 1) == 0) {
				gap++;
				max = Math.max(max, gap);
			} else {
				gap = 0;
			}
			n >>>= 1;
		}
		
		return max;
		
	}
	
	// solution method v2
	public static int solution1(int n) {
		
		String binary = Integer.toBinaryString(n);
		int lastPosition = binary.length() - 1;
		
		System.out.println(binary);
		
		while (binary.charAt(lastPosition) == '0') {
			lastPosition--;
		}
		
		int counter = 0;
		int maxGap = 0;
		
		while (lastPosition >= 0) {
			
			if (binary.charAt(lastPosition) == '1') {
				
				if (counter > maxGap) maxGap = counter;
				
				counter = 0;
				
			} else {
				
				counter++;
				
			}
			
			lastPosition--;
		}
		
		return maxGap;
		
	}

}
