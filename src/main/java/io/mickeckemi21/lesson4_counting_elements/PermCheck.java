package io.mickeckemi21.lesson4_counting_elements;

import java.util.HashSet;

public class PermCheck {

	public static void main(String[] args) {
		
		// testing the FrogRiverOne solution v1
		System.out.println("PermCheck for array [4, 1, 3, 2]: " + solution(new int[] {4, 1, 3, 2}));
		
		// testing the FrogRiverOne solution v1
		System.out.println("PermCheck for array [4, 1, 3]: " + solution(new int[] {4, 1, 3}));
		
		// testing the FrogRiverOne solution v1
		System.out.println("PermCheck for array [1, 1]: " + solution(new int[] {1, 1}));

	}
	
	public static int solution(int[] A) {
		
		HashSet<Integer> values = new HashSet<>();
		int arrayMaxValue = 0;
		
		for (int number : A) {
			arrayMaxValue = Math.max(arrayMaxValue, number);
			values.add(number);
		}
		
		if (values.size() == arrayMaxValue && values.size() == A.length) return 1;
		
		return 0;
		
	}

}
