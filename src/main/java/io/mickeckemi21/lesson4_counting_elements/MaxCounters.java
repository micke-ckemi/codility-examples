package io.mickeckemi21.lesson4_counting_elements;

import java.util.Arrays;

public class MaxCounters {

	public static void main(String[] args) {
		
		// testing the MaxCounters solution v1
		System.out.println("MaxCounters in array {3, 4, 4, 6, 1, 4, 4} for N = 5: " + Arrays.toString(solution(5, new int[] {3, 4, 4, 6, 1, 4, 4})));
		
	}

	public static int[] solution(int N, int[] A) {
		
		int[] counters = new int[N];
		
		int maxCounter = 0;
		int lastUpdate = 0;
		
		for (int i = 0; i < A.length; i++) {
			
			if (A[i] <= N) {
				
				counters[A[i] - 1] = Math.max(lastUpdate, counters[A[i] - 1]) + 1;
				maxCounter = Math.max(maxCounter, counters[A[i] - 1]);
				
			} else {

				lastUpdate = maxCounter;
				
			}
			
		}
		
		for (int i = 0; i < counters.length; i++) counters[i] = Math.max(counters[i], lastUpdate);
	
		
		return counters;
	}

}
