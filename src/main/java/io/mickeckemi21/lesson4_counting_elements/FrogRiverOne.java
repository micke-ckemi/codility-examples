package io.mickeckemi21.lesson4_counting_elements;

import java.util.HashSet;

public class FrogRiverOne {

	public static void main(String[] args) {
		
		// testing the FrogRiverOne solution v1
		System.out.println("FrogRiverOne for array [1, 3, 1, 4, 2, 3, 5, 4]: " + solution(5, new int[] {1, 3, 1, 4, 2, 3, 5, 4}));
		
		// testing the FrogRiverOne solution v1
		System.out.println("FrogRiverOne for array [1, 3, 1, 4, 2, 3, 5, 4, 4, 1, 3, 6, 3, 4, 1, 5]: " + solution(6, new int[] {1, 3, 1, 4, 2, 3, 5, 4, 4, 1, 3, 6, 3, 4, 1, 5}));
		
	}
	
	public static int solution(int X, int[] A) {
		
		HashSet<Integer> positions = new HashSet<>(X);
		
		for (int i = 0; i < A.length; i++) {
		
			positions.add(A[i]);
			
			if (positions.size() == X) {
				return i;
			}
			
		}
		
		return -1;
	}
	
}
