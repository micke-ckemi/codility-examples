package io.mickeckemi21.lesson4_counting_elements;

import java.util.HashSet;

public class MissingInteger {

	public static void main(String[] args) {
		
		// testing the MissingInteger solution v1
		System.out.println("MissingInteger for array [1, 3, 6, 4, 1, 2]: " + solution(new int[] {1, 3, 6, 4, 1, 2}));
		// testing the MissingInteger solution v1
		System.out.println("MissingInteger for array [1, 2, 3]: " + solution(new int[] {1, 2, 3}));
		// testing the MissingInteger solution v1
		System.out.println("MissingInteger for array [-1, -3]: " + solution(new int[] {-1, -3}));

	}
	
	public static int solution(int[] A) {
		
		int missingInteger = 1;
		
		HashSet<Integer> set = new HashSet<>();
//		set.addAll(Arrays.stream(A).boxed().collect(Collectors.toList()));
		for (int number : A) {
			set.add(number);
		}
		
		while(set.contains(missingInteger)) {
			missingInteger++;
		}

		return missingInteger;
	}

}
